package chat_cryption;

import java.io.*;
import java.math.BigInteger;
import java.net.*;
import java.nio.charset.Charset;
public class WriteThread extends Thread {
    private PrintWriter writer;
    private Socket socket;
    private ChatClient client;
    
 
    public WriteThread(Socket socket, ChatClient client) {
        this.socket = socket;
        this.client = client;
 
        try {
            OutputStream output = socket.getOutputStream();
            writer = new PrintWriter(output, true);
        } catch (IOException ex) {
            System.out.println("Error getting output stream: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
 
    public void run() {
 
        Console console = System.console();
 
        String userName = console.readLine("\nEnter your name: ");
        client.setUserName(userName);
        writer.println(userName);
       
        String text;
 
        do {
            text = console.readLine("[" + userName + "]: ");
            if (text.contains("PublicKey = ")) {
            	if (text.split("PublicKey = ").length > 1)
            	{
            		writer.println(text);
            	}               	
            }
            else 
            {
            	if (client.getFriendKey() == BigInteger.valueOf(0))
            	{
            		System.out.println("Not have publickey of friend. Try again late ");
            	} else 
            	{
            		BigInteger m
		            = new BigInteger(
		            		text.getBytes(
		                    Charset.forName("ascii")));
		        BigInteger c = Cryptography.encrypt(m, client.getFriendKey());
            	String cypherText =  c.toString();
            	System.out.println("CypherText :" + cypherText);
            	writer.println(cypherText);
            	}          	
            }           
        } while (!text.equals("bye"));
 
        try {
            socket.close();
        } catch (IOException ex) {
 
            System.out.println("Error writing to server: " + ex.getMessage());
        }
    }
}
