package chat_cryption;

import java.net.*;
import java.io.*;
import java.math.BigInteger;
 
public class ChatClient {
    private String hostname;
    private int port;
    private String userName;
    private BigInteger n;
    private BigInteger p;
    private BigInteger q;
    private BigInteger friendKey = BigInteger.valueOf(0);
 
    public ChatClient(String hostname, int port) {
        this.hostname = hostname;
        this.port = port;
        BigInteger[] key = Cryptography.generateKey(512);
        this.n = key[0];
        this.p = key[1];
        this.q = key[2];
    }
 
    public void execute() {
        try {
            Socket socket = new Socket(hostname, port);
 
            System.out.println("Connected to the chat server");  
            System.out.println("PublicKey = "+ this.n);
            System.out.println("PrivateKey = { p = "+ this.p + "; q = "+ this.q + " }");
            new ReadThread(socket, this).start();
            new WriteThread(socket, this).start();
 
        } catch (UnknownHostException ex) {
            System.out.println("Server not found: " + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("I/O Error: " + ex.getMessage());
        }
 
    }
 
    void setUserName(String userName) {
        this.userName = userName;
    }
 
    String getUserName() {
        return this.userName;
    }
    
    void setFriendKey(BigInteger key) {
        this.friendKey = key;
    }
 
    BigInteger getFriendKey() {
        return this.friendKey;
    }
    
    BigInteger getPublicKey()
    {
    	return this.n;
    }
    BigInteger getP()
    {
    	return this.p;
    }
    BigInteger getQ()
    {
    	return this.q;
    }
 
    public static void main(String[] args) {
        if (args.length < 2) return;
 
        String hostname = args[0];
        int port = Integer.parseInt(args[1]);
 
        ChatClient client = new ChatClient(hostname, port);
        client.execute();
    }
}

/** java -Dfile.encoding=Cp1252 -classpath "C:\Users\DoPhong\eclipse-workspace\chat_cryption\bin" chat_cryption.ChatClient localhost 8989
 * 
 * java -Dfile.encoding=Cp1252 -classpath "C:\Users\DoPhong\eclipse-workspace\chat_cryption\bin" chat_cryption.ChatServer 8989 */ 