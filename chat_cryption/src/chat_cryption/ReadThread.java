package chat_cryption;

import java.io.*;
import java.math.BigInteger;
import java.net.*;
import java.nio.charset.Charset;
public class ReadThread extends Thread {
    private BufferedReader reader;
    private Socket socket;
    private ChatClient client;
    
    public ReadThread(Socket socket, ChatClient client) {
        this.socket = socket;
        this.client = client;
 
        try {
            InputStream input = socket.getInputStream();
            reader = new BufferedReader(new InputStreamReader(input));
        } catch (IOException ex) {
            System.out.println("Error getting input stream: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
 
    public void run() {
        while (true) {
            try {
                String response = reader.readLine();
                
                System.out.println("\n" + response);
                if (response.contains("PublicKey = ")) {             	
                	if (response.split("PublicKey = ").length > 1)
                	{
                		client.setFriendKey(new BigInteger(response.split("PublicKey = ")[1].toString()));
                	}               	
                } else 
                {
                	if (response.contains("]: "))
                	{
                		if (response.split("]: ").length > 1)
                    	{              			
                        	BigInteger c = new BigInteger(response.split("]: ")[1].toString());
                        	BigInteger[] m2 = Cryptography.decrypt(c, client.getP(), client.getQ());
            		        for (BigInteger b : m2) {
            		            String dec = new String(
            		                b.toByteArray(),
            		                Charset.forName("ascii"));
            		            System.out.println("\n Plaintext text :" + dec);
            		        }
                    	} else 
                    	{
                    		System.out.println("\n" + response);
                    	}
                	}
                	
                }
                
                // prints the username after displaying the server's message
                if (client.getUserName() != null) {
                    System.out.print("[" + client.getUserName() + "]: ");
                }
            } catch (IOException ex) {
                System.out.println("Error reading from server: " + ex.getMessage());
                ex.printStackTrace();
                break;
            }
        }
    }
}
